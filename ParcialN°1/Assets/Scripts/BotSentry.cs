using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotSentry : MonoBehaviour
{
    public int vida = 150;
    private GameObject jugador;
    public GameObject zonaDisparo;
    public GameObject bala_enemiga;
    public GameObject zonaDeteccion;
    public float tiempoDisparo = 1;
    private float tiempoAux;
    private bool deteccion = false;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        tiempoAux = tiempoDisparo;
    }
    void Update()
    {
        transform.LookAt(jugador.transform);
    }
    private void FixedUpdate()
    {
        if (deteccion)
        {
            tiempoDisparo -= Time.deltaTime;
            if (tiempoDisparo < 0)
            {
                Disparo();
                tiempoDisparo = tiempoAux;
            }
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            RecibirDaņo();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            deteccion = true;
        }   
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            deteccion = false;
        }
    }
    private void Disparo()
    {
        GameObject proyectilactual = Instantiate(bala_enemiga, zonaDisparo.transform.position, zonaDisparo.transform.rotation);
        Rigidbody rbbalaenemiga = proyectilactual.GetComponent<Rigidbody>();

        rbbalaenemiga.AddForce(transform.forward * 15f, ForceMode.Impulse);
        Destroy(proyectilactual, 5);
    }
    private void RecibirDaņo()
    {
        vida -= 25;

        if (vida == 0)
        {
            Destroy(gameObject);
        }
    }
}
