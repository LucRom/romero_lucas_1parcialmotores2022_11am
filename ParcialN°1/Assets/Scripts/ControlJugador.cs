using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public int rapidez = 5;
    public int velocidad_salto = 5;
    private Rigidbody rb;
    private bool salto = false;
    private int cant_saltos = 0;
    private bool powerUp = false;
    private float tiempoPowerUp = 15;
    public int vida = 100;
    private int vidaaux = 0;
    public TMPro.TMP_Text txtPowerUp;
    public TMPro.TMP_Text txtVida;
    public Camera camara;
    public GameObject bala;
    public bool gameover = false;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        txtPowerUp.text = "";
        txtVida.text = "Vida: " + vida.ToString();
    }
    void Update()
    {
        //Movimiento
        float movimentoAD = Input.GetAxis("Horizontal") * rapidez;
        float movimientoWS = Input.GetAxis("Vertical") * rapidez;

        transform.Translate(new Vector3(movimentoAD, 0f, movimientoWS) * Time.deltaTime);

        //Cursor
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        //Salto

        if (Input.GetKeyDown(KeyCode.Space))
        {
            salto = true;
        }

        //PowerUp

        if (powerUp)
        {
            StartCoroutine(ContPowerUp(15));
            powerUp = false;
        }

        if (tiempoPowerUp == 0)
        {
            vida = vidaaux;
            transform.localScale = new Vector3(1, 1, 1);
            txtVida.text = "Vida: " + vida.ToString();
            rapidez = 5;
            txtPowerUp.text = "";
        }

        //Ray Casting y disparo
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            GameObject proyectil;
            proyectil = Instantiate(bala, ray.origin, transform.rotation);
            Rigidbody rbbala = proyectil.GetComponent<Rigidbody>();
            rbbala.AddForce(camara.transform.forward * 15, ForceMode.Impulse);

            Destroy(proyectil, 5);
        }
    }
    private void FixedUpdate()
    {
        //Para que no saltes infinitamente
        if (cant_saltos == 2)
        {
            return;
        }

        //Comandos del Rigidbody
        if (salto)
        {
            rb.AddForce(new Vector3(0, velocidad_salto, 0), ForceMode.Impulse);
            salto = false;
            cant_saltos++;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        cant_saltos = 0;
        if (collision.gameObject.name.Substring(0,3) == "Bot")
        {
            RecibirDaņoJugador();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            transform.localScale = new Vector3(1.5f, 1.5f, 1);
            rapidez *= 2;
            vidaaux = vida;
            vida *= 2;
            TextoVida();
            other.gameObject.SetActive(false);
            powerUp = true;
        }
        if (other.gameObject.CompareTag("Vida"))
        {
            vida += 25;
            TextoVida();
            other.gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("EndGame"))
        {
            gameover = true;
        }
        if (other.gameObject.CompareTag("Bala_enemigo"))
        {
            RecibirDaņoJugador();
        }
    }
    public IEnumerator ContPowerUp(float cronometro = 15)
    {
        tiempoPowerUp = cronometro;
        while (tiempoPowerUp >= 0)
        {
            TextoPowerUp();
            Debug.Log("Restan " + tiempoPowerUp + " Segs para volver a tu estado normal");
            yield return new WaitForSeconds(1.0f);
            tiempoPowerUp--;
        }
    }

    private void TextoPowerUp()
    {
        txtPowerUp.text = "PowerUp: " + tiempoPowerUp.ToString() + " Segs";
    }

    public void TextoVida()
    {
        txtVida.text = "Vida: " + vida.ToString();
    }
    public void RecibirDaņoJugador()
    {
        vida -= 25;
        TextoVida();
    }
}

