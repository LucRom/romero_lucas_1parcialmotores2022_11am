using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotDistancia : MonoBehaviour
{
    public int vida = 100;
    public float velocidad = 0.5f;
    private GameObject jugador;
    public GameObject zonaDisparo;
    public GameObject bala_enemiga;
    public float tiempoDisparo = 5;
    private float tiempoAux;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        tiempoAux = tiempoDisparo;
    }
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);;
    }
    private void FixedUpdate()
    {
        tiempoDisparo -= Time.deltaTime;
        if (tiempoDisparo < 0)
        {
            Disparo();
            tiempoDisparo = tiempoAux;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            RecibirDaņo();
        }
    }
    private void Disparo()
    {
        GameObject proyectilactual = Instantiate(bala_enemiga, zonaDisparo.transform.position, zonaDisparo.transform.rotation);
        Rigidbody rbbalaenemiga = proyectilactual.GetComponent<Rigidbody>();

        rbbalaenemiga.AddForce(transform.forward * 15f, ForceMode.Impulse);
        Destroy(proyectilactual, 1);
    }
    private void RecibirDaņo()
    {
        vida -= 25;

        if (vida == 0)
        {
            Destroy(gameObject);
        }
    }
}
