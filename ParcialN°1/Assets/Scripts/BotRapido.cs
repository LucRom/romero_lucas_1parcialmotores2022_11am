using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotRapido : MonoBehaviour
{
    public int vida = 100;
    public float velocidad = 5;
    private GameObject jugador;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            RecibirDaņo();
        }
        if (collision.gameObject.CompareTag("Jugador"))
        {
            TpJugador();
        }
    }
    private void TpJugador()
    {
        jugador.transform.position = new Vector3(0f, 2.86f, 0f);
    }
    private void RecibirDaņo()
    {
        vida -= 25;

        if (vida == 0)
        {
            Destroy(gameObject);
        }
    }
}
