using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public TMPro.TMP_Text txtTemporizador;
    public TMPro.TMP_Text txtEstadoDeJuego;
    public GameObject jugador;
    public GameObject botMelee;
    public GameObject botTP;
    public GameObject botTanque;
    public GameObject botTorreta;
    public GameObject botDistancia;
    public GameObject powerUp;
    public GameObject powerUpVida;
    private float contadorJuego = 60;
    ControlJugador vidaJugador;
    private List<GameObject> listaenemigos = new List<GameObject>();
    private List<GameObject> powerups = new List<GameObject>();
    void Start()
    {
        txtEstadoDeJuego.text = "";
        ComenzarJuego();
        vidaJugador = (ControlJugador)jugador.GetComponent<ControlJugador>();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ComenzarJuego();
        }
        if (jugador.transform.position.y <= -10)
        {
            ComenzarJuego();
        }
        if (contadorJuego == 0)
        {
            Time.timeScale = 0;
            txtEstadoDeJuego.text = "GAME OVER. R para reiniciar";
        }
        if (vidaJugador.vida == 0)
        {
            Time.timeScale = 0;
            txtEstadoDeJuego.text = "Has muerto. R para reiniciar";
        }
        if (vidaJugador.gameover)
        {
            Time.timeScale = 0;
            txtEstadoDeJuego.text = "Has Ganado. R para reiniciar";
        }

    }
    void ComenzarJuego()
    {
        StopAllCoroutines();
        vidaJugador = (ControlJugador)jugador.GetComponent<ControlJugador>();
        txtEstadoDeJuego.text = "";
        vidaJugador.vida = 100;
        vidaJugador.gameover = false;
        vidaJugador.TextoVida();
        Time.timeScale = 1;

        foreach (GameObject item in listaenemigos)
        {
            Destroy(item);
        }

        jugador.transform.position = new Vector3(0f, 2.86f, 0f);

        listaenemigos.Add(Instantiate(botMelee, new Vector3(7.22f, 0.809f, -8f), Quaternion.identity));
        listaenemigos.Add(Instantiate(botTP, new Vector3(7.76f, 0.809f, 8f), Quaternion.identity));
        listaenemigos.Add(Instantiate(botTanque, new Vector3(-7.11f, 1.44f, -8f), Quaternion.identity));
        listaenemigos.Add(Instantiate(botDistancia, new Vector3(-7.82f, 1.53f, 8f), Quaternion.identity));
        listaenemigos.Add(Instantiate(botTorreta, new Vector3(-3.89f, 3.28f, 19.88f), Quaternion.identity));

        powerups.Add(Instantiate(powerUpVida, new Vector3(-9f, 0.69f, 0.31f), Quaternion.identity));
        powerups.Add(Instantiate(powerUpVida, new Vector3(7.75f, 0.69f, 0.31f), Quaternion.identity));
        powerups.Add(Instantiate(powerUp, new Vector3(0.48f, 0.69f, 8.49f), Quaternion.identity));
        powerups.Add(Instantiate(powerUp, new Vector3(-0.03f, 0.69f, -8.49f), Quaternion.identity));

        Debug.Log("Se a reiniciado el Juego");
        StartCoroutine(TempJuego(60));
    }
    private void TxtTemp()
    {
        txtTemporizador.text = "Tiempo restante: " + contadorJuego.ToString();
    }
    public IEnumerator TempJuego(float cronometro = 60)
    {
        contadorJuego = cronometro;
        while (contadorJuego >= 0)
        {
            TxtTemp();
            yield return new WaitForSeconds(1.0f);
            contadorJuego--;
        }
    }
}