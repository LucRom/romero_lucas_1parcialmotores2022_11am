using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMelee : MonoBehaviour
{
    public int vida = 100;
    public float velocidad = 5;
    private GameObject jugador;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }
    void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            RecibirDaņo();
        }
    }
    private void RecibirDaņo()
    {
        vida -= 25;

        if(vida == 0)
        {
            Destroy(gameObject);
        }
    }

}
